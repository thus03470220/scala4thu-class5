/**
  * Created by mark on 26/03/2017.
  */
object Exercises {
  def sortByValue(kvs:List[(Int, String)])={
    kvs.sortBy(kv=>kv._2)
  }

  def accumulator(nums:List[Int])={
    nums.foldLeft(List(0))((acc,x)=>{
      acc:+(acc.last)+x
    }).tail
  }

  def wordCount(fruits:List[String])={
    words.groupBy(str=>str).mapValues(_.length)
  }



}
